/*
 *  Copyright 2008-2010, 2018 Jerome PASQUIER
 * 
 *  This file is part of glAdventure - A little game using XGL.
 *
 *  glAdventure is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  glAdventure is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with glAdventure; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 *  02110-1301  USA
 *
 */

#include "gsname.h"

#include "opengl.h"
#include "gslist.h"  // Uses GS_INTRO
#include "logger.h"  // Uses LOG

game_state_t*
init_gs_name(void)
{
  game_state_t* gs = new_game_state("GS Name)", &draw_gs_name);
  gs->key_list = add_binding(gs->key_list, XK_Escape, &return_to_intro);
  return gs;
}

void
free_gs_name(game_state_t* gs)
{
  free_game_state(gs);
}

int
draw_gs_name(void)
{
  // Reset color and texture
  glColor3f(1.0f, 1.0f, 1.0f);
  //  glBindTexture(GL_TEXTURE_2D, texture[1]);

  // Set z-order to less than -1.0 to be in foreground of the game UI
  // (obejct list and map name)
  float z_order = -1.0f;
  float x1 = -0.2f;
  float x2 = 0.2f;
  float y1 = 0.20f;
  float y2 = 0.31;
  
  // Light effect
  GLfloat le = 0.3f;

  glBegin(GL_QUADS);
    glColor4f(0.5f, 0.5f, 0.7f, 0.7f);
    glVertex3f( x1, y1, z_order);
    glVertex3f( x1, y2, z_order);	
 
    glColor4f(0.5f + le, 0.5f + le, 0.7f + le, 0.7f);
    glVertex3f( x2, y2, z_order);	
    glVertex3f( x2, y1, z_order);	
  glEnd();	
  
}

/** Return to the intro gamestate
  *
  * \return The change state value with gintro as parameter
  *
  */
int return_to_intro(void){
  LOG("return_to_intro called");
  return ((GS_INTRO << GS_CHST_BIT_SHIFT) +6);
}
