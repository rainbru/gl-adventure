/*
 *  Copyright 2008-2010, 2018 Jerome PASQUIER
 * 
 *  This file is part of glAdventure - A little game using XGL.
 *
 *  glAdventure is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  glAdventure is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with glAdventure; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 *  02110-1301  USA
 *
 */

/** \file widget_list.c
  * Widget list implementation.
  *
  */

#include <stdlib.h>

#include "widget_list.h"

widget_list_t*
widget_list_create(void)
{
  widget_list_t* sl = (widget_list_t*)malloc(sizeof(widget_list_t));
  sl->start = NULL;

  return sl;
  
}

void
widget_list_free(widget_list_t** wl)
{
  free(*wl);
}

int
widget_list_len(widget_list_t** l)
{
  int len = 0;
  widget_t* it = (*l)->start;
  while (it !=NULL)
    len++;
  return len;
}

void
widget_list_add(widget_list_t** l, widget_t* w)
{
  widget_t* it = (*l)->start;
  while (it !=NULL)
    it = it->next;

  it-> next = w;
  w->next = NULL;
}
