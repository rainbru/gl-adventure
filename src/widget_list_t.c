/*
 *  Copyright 2008-2010, 2018 Jerome PASQUIER
 * 
 *  This file is part of glAdventure - A little game using XGL.
 *
 *  glAdventure is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  glAdventure is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with glAdventure; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 *  02110-1301  USA
 *
 */

#include <check.h>

#include "widget_list_t.h"
#include "widget_list.h"

/// Test if instantiating a new list do not return NULL
START_TEST (test_wl_not_null)
{
  widget_list_t* wl = widget_list_create();
  ck_assert(wl != NULL);
}
END_TEST

START_TEST (test_wl_dtor)
{
  widget_list_t* wl = widget_list_create();
  widget_list_free(&wl);
  ck_assert(wl == NULL);
}
END_TEST

START_TEST (test_wl_len)
{
  widget_list_t* wl = widget_list_create();
  ck_assert( widget_list_len(&wl) == 0);
  widget_list_free(&wl);
}
END_TEST

START_TEST (test_wl_add)
{
  widget_t *w = NULL, *w2 = NULL, *w3 = NULL;
  widget_list_t* wl = widget_list_create();
  widget_list_add(&wl, w);
  ck_assert( widget_list_len(&wl) == 1);
  widget_list_add(&wl, w2);
  ck_assert( widget_list_len(&wl) == 2);
  widget_list_add(&wl, w3);
  ck_assert( widget_list_len(&wl) == 3);
  widget_list_free(&wl);
}
END_TEST

Suite * widget_list_suite(void)
{
    Suite* s = suite_create("WidgetList");

    TCase* tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_wl_not_null);
    suite_add_tcase(s, tc_core);

    return s;
}

