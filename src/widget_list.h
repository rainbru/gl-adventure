/*
 *  Copyright 2008-2010, 2018 Jerome PASQUIER
 * 
 *  This file is part of glAdventure - A little game using XGL.
 *
 *  glAdventure is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  glAdventure is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with glAdventure; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 *  02110-1301  USA
 *
 */

/** \file widget_list.h
  * Header file for the widget list.
  *
  */

#ifndef _WIDGET_LIST_H_
#define _WIDGET_LIST_H_

typedef struct widget {
  void *  empty;
  struct widget* next; 
} widget_t;

typedef struct{
  widget_t* start; 
} widget_list_t;

widget_list_t* widget_list_create(void);
void         widget_list_free(widget_list_t**);

int   widget_list_len(widget_list_t**);
void  widget_list_add(widget_list_t**, widget_t*);


#endif // !_WIDGET_LIST_H_
