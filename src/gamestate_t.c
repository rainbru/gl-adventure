/*
 *  Copyright 2008-2010, 2018 Jerome PASQUIER
 * 
 *  This file is part of glAdventure - A little game using XGL.
 *
 *  glAdventure is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  glAdventure is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with glAdventure; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 *  02110-1301  USA
 *
 */

#include <check.h>

#include "gamestate_t.h"
#include "gamestate.h"

START_TEST (test_gamestate_not_null)
{
  ck_assert(NULL != new_game_state("test", NULL));
}
END_TEST

// Test that the widget lists is not NULL
START_TEST (test_gamestate_widgets_not_null)
{
  game_state_t* gs = new_game_state("test", NULL);
  ck_assert(NULL != gs->widgets);
}
END_TEST


Suite * gamestate_suite(void)
{
    Suite* s = suite_create("Loadmap");

    TCase* tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_gamestate_not_null);
    tcase_add_test(tc_core, test_gamestate_widgets_not_null);

    suite_add_tcase(s, tc_core);

    return s;
}
